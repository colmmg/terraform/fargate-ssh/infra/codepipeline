{
  "Statement":[
    {
      "Action":[
        "iam:PassRole"
      ],
      "Resource": [
        "*"
      ],
      "Effect":"Allow",
      "Condition":{
        "StringEqualsIfExists":{
          "iam:PassedToService":[
            "ecs-tasks.amazonaws.com"
          ]
        }
      }
    },
    {
      "Action":[
        "codecommit:CancelUploadArchive",
        "codecommit:GetBranch",
        "codecommit:GetCommit",
        "codecommit:GetUploadArchiveStatus",
        "codecommit:UploadArchive"
      ],
      "Resource":"*",
      "Effect":"Allow"
    },
    {
      "Action":[
        "codedeploy:CreateDeployment",
        "codedeploy:GetApplication",
        "codedeploy:GetApplicationRevision",
        "codedeploy:GetDeployment",
        "codedeploy:GetDeploymentConfig",
        "codedeploy:RegisterApplicationRevision"
      ],
      "Resource":"*",
      "Effect":"Allow"
    },
    {
      "Action":[
        "cloudwatch:*",
        "ecs:*"
      ],
      "Resource":"*",
      "Effect":"Allow"
    },
    {
      "Action":[
        "codebuild:BatchGetBuilds",
        "codebuild:StartBuild"
      ],
      "Resource":"*",
      "Effect":"Allow"
    },
    {
      "Effect":"Allow",
      "Resource":[
        "arn:aws:s3:::${codepipeline-artifacts-bucket}",
        "arn:aws:s3:::${codepipeline-artifacts-bucket}/*"
      ],
      "Action":[
        "s3:PutObject",
        "s3:GetObject",
        "s3:GetObjectVersion",
        "s3:GetBucketAcl",
        "s3:GetBucketLocation"
      ]
    },
    {
      "Effect":"Allow",
      "Action":[
        "ecr:DescribeImages"
      ],
      "Resource":"*"
    }
  ],
  "Version":"2012-10-17"
}
