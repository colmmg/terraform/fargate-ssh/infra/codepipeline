data "aws_iam_policy_document" "codebuild" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codebuild" {
  assume_role_policy = data.aws_iam_policy_document.codebuild.json
  name               = "CodeBuildServiceRole"
  path               = "/service-role/"
}

data "template_file" "codebuild" {
  template = file("${path.module}/templates/codebuild-policy.json.tpl")
  vars = {
    aws_account_id                = data.aws_caller_identity.current.account_id
    codebuild-artifacts-bucket    = aws_s3_bucket.codebuild-artifacts.id
    codepipeline-artifacts-bucket = aws_s3_bucket.codepipeline-artifacts.id
    region                        = var.region
  }
}

resource "aws_iam_role_policy" "codebuild" {
  name   = "CodeBuildServiceRole"
  policy = data.template_file.codebuild.rendered
  role   = aws_iam_role.codebuild.id
}
