data "aws_iam_policy_document" "codepipeline" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codepipeline.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codepipeline" {
  assume_role_policy = data.aws_iam_policy_document.codepipeline.json
  name               = "CodePipelineServiceRole"
  path               = "/service-role/"
}

data "template_file" "codepipeline" {
  template = file("${path.module}/templates/codepipeline-policy.json.tpl")
  vars = {
    codepipeline-artifacts-bucket = aws_s3_bucket.codepipeline-artifacts.id
  }
}

resource "aws_iam_role_policy" "codepipeline" {
  name   = "CodePipelineServiceRole"
  policy = data.template_file.codepipeline.rendered
  role   = aws_iam_role.codepipeline.id
}
