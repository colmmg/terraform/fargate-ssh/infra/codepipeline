resource "aws_s3_bucket" "codebuild-artifacts" {
  acl           = "private"
  bucket        = "codebuild-artifacts-${var.region}-${data.aws_caller_identity.current.account_id}"
  lifecycle_rule {
    prefix  = ""
    enabled = true
    noncurrent_version_expiration {
      days = 7
    }
  }
  lifecycle_rule {
    prefix  = ""
    enabled = true
    expiration {
      days = 30
    }
  }
  region = var.region
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
    }
  }
  tags = {
    Name    = "codebuild-artifacts"
  }
  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket" "codepipeline-artifacts" {
  acl           = "private"
  bucket        = "codepipeline-artifacts-${var.region}-${data.aws_caller_identity.current.account_id}"
  lifecycle_rule {
    prefix  = ""
    enabled = true
    noncurrent_version_expiration {
      days = 7
    }
  }
  lifecycle_rule {
    prefix  = ""
    enabled = true
    expiration {
      days = 30
    }
  }
  region = var.region
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
    }
  }
  tags = {
    Name    = "codepipeline-artifacts"
  }
  versioning {
    enabled = true
  }
}
