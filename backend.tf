terraform {
  backend "s3" {
    bucket = "tf-state-us-east-1-123456789012"
    key    = "infra/codepipeline/terraform.tfstate"
    region = "us-east-1"
  }
}
