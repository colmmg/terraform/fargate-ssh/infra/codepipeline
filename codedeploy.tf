data "aws_iam_policy" "codedeploy" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
}

data "aws_iam_policy_document" "codedeploy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codedeploy.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codedeploy" {
  assume_role_policy = data.aws_iam_policy_document.codedeploy.json
  name               = "CodeDeployServiceRole"
  path               = "/service-role/"
}

resource "aws_iam_role_policy_attachment" "codedeploy" {
  policy_arn = data.aws_iam_policy.codedeploy.arn
  role       = aws_iam_role.codedeploy.name
}

data "template_file" "codedeploy" {
  template = file("${path.module}/templates/codedeploy-policy.json.tpl")
  vars = {
    codepipeline-artifacts-bucket = aws_s3_bucket.codepipeline-artifacts.id
  }
}

resource "aws_iam_role_policy" "codedeploy" {
  name   = "CodeDeployServiceRole"
  policy = data.template_file.codedeploy.rendered
  role   = aws_iam_role.codedeploy.id
}
