output "codebuild-artifacts-bucket-id" {
  value = aws_s3_bucket.codebuild-artifacts.id
}

output "codepipeline-artifacts-bucket-id" {
  value = aws_s3_bucket.codepipeline-artifacts.id
}

output "codebuild-role-arn" {
  value = aws_iam_role.codebuild.arn
}

output "codedeploy-role-arn" {
  value = aws_iam_role.codedeploy.arn
}

output "codepipeline-role-arn" {
  value = aws_iam_role.codepipeline.arn
}
